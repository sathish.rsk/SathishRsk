import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MyServiceService {
  private myData:string[] = ['sathish','selva','santhosh','antobenard','vedha'];
  constructor() { }
  getData():string[]{
    return this.myData;
  }
}
