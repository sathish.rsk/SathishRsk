import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { FirstCompComponent } from './component/first-comp/first-comp.component';
import { SlideShowComponent } from './component/slide-show/slide-show.component';
import { InputTestComponent } from './component/input-test/input-test.component';
import { MyDirectiveDirective } from './my-directive.directive';
import { MyPipePipe, FilterPipe } from './my-pipe.pipe';

@NgModule({
  declarations: [
    AppComponent,
    FirstCompComponent,
    SlideShowComponent,
    InputTestComponent,
    MyDirectiveDirective,
    MyPipePipe,
    FilterPipe
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 
    
}
