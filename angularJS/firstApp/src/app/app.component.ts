import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  onclick():void{
    this.title = "hello "+this.title;
  }

  title = "firstApp" ;
  dbz: string[] = [
  "https://pmcdeadline2.files.wordpress.com/2015/08/dbzf47.png?w=446&h=299&crop=1",
  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT4Tb1vJq72gCCCbTwm1FWaZaCj5aNqr3nK_qxW65DI5r5nwYwu",
  "https://i.ebayimg.com/images/g/n~kAAOSwl8NVZwxB/s-l300.jpg"
  ];
  onepiece: string[] = [
  "https://otakukart.com/wp-content/uploads/2018/02/C_116_fotogallery_1949_lstFoto_foto_1_upiFoto.jpg",
  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRhLrl7h4Ez0nYfYMNLM2G316q2cwAGFJq_JbM-tpzp97RutwcU",
  "https://sotaku.com/wp-content/uploads/2017/09/eiichiro-oda-wants-to-end-one-piece-as-soon-as-possible.jpg"
  ];
}
