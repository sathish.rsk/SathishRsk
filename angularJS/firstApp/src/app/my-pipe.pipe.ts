import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'myPipe'
})
export class MyPipePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if(args > 50)
      return value+" senior";
    else
      return value +" junior";
  }

}

@Pipe({
  name: 'Filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value;
  }

}
