import { Component, OnInit } from '@angular/core';
interface Cuser{
	name:string,
	age:number
}
@Component({
  selector: 'app-first-comp',
  templateUrl: './first-comp.component.html',
  styleUrls: ['./first-comp.component.css']
})
export class FirstCompComponent implements OnInit {
	details:Cuser[] = [
		{
			name:"sathishKumar S",
			age:10
		},
		{
			name:"santhosh King",
			age:90
		},
		{
			name:"anto",
			age:25
		}
	];
	myArray:Cuser[] = [...this.details] ;
	myclick:Cuser;
	inputData:Cuser[] = this.clearInputData();
	constructor() { }
	clearInputData():Cuser[]{
		return [{
			name:"",
			age:null
		}]
	}
	cusFilter(arg:Cuser):Cuser[]{
		return this.myArray.filter(function(index){
					return index.name != arg.name || index.age != index.age ;
				});
	}
	onMyClick(x: Cuser){
		this.myArray = this.cusFilter(x);
	}
	
	putData(){
		this.myArray = [...this.myArray , ...this.inputData];
		this.inputData = this.clearInputData();
	}

	ngOnInit() {
	}

}
