import { Component, OnInit , Input } from '@angular/core';
import { MyServiceService } from '../../my-service.service';
//import { FilterPipe } from '../../my-pipe.pipe';

@Component({
  selector: 'app-slide-show',
  templateUrl: './slide-show.component.html',
  styleUrls: ['./slide-show.component.css']
})
export class SlideShowComponent implements OnInit {
	@Input()
  	imageUrl: string[] = [];
  	imgptr:number = 0;

  	constructor(public cusService:MyServiceService ) { }
  	slideFunc(x:number){
      if(x > 0){
            if(this.imgptr < 2)
                this.imgptr += x;
            else{
				this.imgptr += x;
				this.imgptr %= this.imageUrl.length;
			}
      }else{
            if(this.imgptr > 0)
                this.imgptr += x;
            else
                this.imgptr = this.imageUrl.length -1;
      }
	}
	cusColor:string = "red";
	inputData = "";
	totalList:string[] = this.cusService.getData();
	myList:string[] = [...this.totalList];
	valuechange(reg){
		this.myList = this.cusService.getData()
		.filter(function(item){
			return item.indexOf(reg) > -1;
		});
	}
	ngOnInit() {
	}

}
