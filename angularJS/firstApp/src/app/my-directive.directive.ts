import { Directive ,ElementRef , Input, HostListener} from '@angular/core';

@Directive({
	selector: '[appMyDirective]'
})
export class MyDirectiveDirective {
	@Input()
	cusdir:string;
	@HostListener('click')
	onclick(){
		alert("clicked");
		this.e.nativeElement.style.color = "red";
	}
	constructor(private e: ElementRef) {
	}
	ngOnInit(){
	}
}
