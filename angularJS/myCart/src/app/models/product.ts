export default interface Iproduct{
        _id:string;
        name:string;
        company:string;
        description:string;
        price:number;
        imageUrl:string;
}