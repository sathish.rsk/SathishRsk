import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import Iproduct from './models/product';
interface Icart{
	pdt : Iproduct,
	count : number
}
@Injectable({
  providedIn: 'root'
})
export class AppService {

	myCheckOuts :Icart[] = [];
	public viewChangeEvent: Subject<String> = new Subject<String>();
	constructor() { }
	addCheckOut(product :Iproduct) : void{
		var temp : Icart = {
			"pdt" : null,
			"count" : null
		};
		if(!this.checkDataInCart(product)){
			temp["pdt"] = product;
			temp["count"] = 1;
			this.myCheckOuts.push(temp);
		}
	}
	checkDataInCart(product :Iproduct) :boolean{
		var count:Number = 0 ;
		var result = this.myCheckOuts.find(data => {
			return data.pdt._id == product._id;
		});
		return (result) ? true : false;
	}
	getTotalCount(){
		return this.myCheckOuts.length;
	}
	getTotalCheckedItems(){
		return this.myCheckOuts;
	}
}
