import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

 
import { CartModule } from './cart/cart.module';
import { ProductModule } from './product/product.module';
import { AuthModule } from './auth/auth.module'

import { AppComponent } from './app.component';

import { AppRoutingModule } from './app-routing.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    AuthModule,
    CartModule,
    ProductModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})  
export class AppModule { }
