import { Component, OnInit } from '@angular/core';

import { CartServiceService } from '../../cart-service.service';

@Component({
  selector: 'app-cart-list',
  templateUrl: './cart-list.component.html',
  styleUrls: ['./cart-list.component.css']
})
export class CartListComponent implements OnInit {

	myCheckOut ;
	displayData;
	constructor(private service : CartServiceService) { }

	ngOnInit() {
		this.myCheckOut = this.service.getCheckedItems();
	}

}
