import { Component, OnInit } from '@angular/core';

import { CartServiceService } from '../../cart-service.service';

@Component({
	selector: 'app-mini-cart',
	templateUrl: './mini-cart.component.html',
	styleUrls: ['./mini-cart.component.css']
})
export class MiniCartComponent implements OnInit {
	itemCount = null;
	constructor(private cartService: CartServiceService) { }

	ngOnInit() {
		this.itemCount = this.cartService.getItemCount();
	}

}
