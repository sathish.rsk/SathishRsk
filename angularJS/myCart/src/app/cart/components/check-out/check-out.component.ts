import { Component, OnInit } from '@angular/core';

import { CartServiceService } from '../../cart-service.service';

@Component({
  selector: 'app-check-out',
  templateUrl: './check-out.component.html',
  styleUrls: ['./check-out.component.css']
})
export class CheckOutComponent implements OnInit {

  myCheckOut;
  constructor(private cartService:CartServiceService) { }

    ngOnInit() {
        this.myCheckOut = this.cartService.getCheckedItems();
    }

}
