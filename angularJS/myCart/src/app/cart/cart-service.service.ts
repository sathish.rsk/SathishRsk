import { Injectable } from '@angular/core';
import { AppService } from '../app.service';

@Injectable({
  	providedIn: 'root'
})
export class CartServiceService {
	myData ;
	constructor(private appService:AppService) { }

	getCheckedItems(){
		this.myData = this.appService.getTotalCheckedItems();
		return this.myData;
	}
	getItemCount(){
		return this.appService.getTotalCount();
	}
}
