import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MiniCartComponent } from './components/mini-cart/mini-cart.component';
import { CartListComponent } from './components/cart-list/cart-list.component';
import { CheckOutComponent } from './components/check-out/check-out.component';

import { AppRoutingModule } from '../app-routing.module';

@NgModule({
  imports: [
    CommonModule,
    AppRoutingModule
  ],
  declarations: [
    MiniCartComponent, 
    CartListComponent, 
    CheckOutComponent
  ],
  exports:[
    MiniCartComponent, 
    CartListComponent, 
    CheckOutComponent
  ]
})
export class CartModule { }
