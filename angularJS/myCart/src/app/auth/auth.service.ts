import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable ,Subject , of} from 'rxjs';

import  { HttpClient ,HttpHeaders } from '@angular/common/http';
import   Iuser  from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
	private myRes:object;
	public logEvent :Subject<boolean> = new Subject<boolean>();
	private myUrl = "http://localhost:8090/api/myCart/";
	private httpOptions = {
		headers: new HttpHeaders({ 'Content-Type': 'application/json' })
	};

	constructor(private router : Router, private http : HttpClient ) { }
	login(fromData) {
		this.http.post<Iuser[]>(this.myUrl+"logIn" , fromData , this.httpOptions)
			.subscribe(data => {
				if(data.length != 0){
					localStorage.setItem('auth',data[0]._id);
					this.logEvent.next(true);
					this.router.navigate(['/','products']);
				}else{

				}
		});
		
		// const res = this.users.find(data =>{
		// 		return data.email == fromData.email && data.pass == fromData.password;
		// });
		// if(res){
		// 	localStorage.setItem('auth',fromData.email);
		// 	this.logEvent.next(true);
		// 	this.router.navigate(['/','products']);
		// }
	}
	isLoggedIn(){
		return localStorage.getItem('auth') == null ? false : true;
	}
	logout(){
		this.logEvent.next(false);
		localStorage.clear();
	}
	signUp(fromData){
		this.http.post<Iuser[]>(this.myUrl+"signUp" , fromData , this.httpOptions)
			.subscribe(data => {
				this.login(data[0]);
		});
	}
	verifyEmail(arg){
		return this.http.get<Iuser[]>(this.myUrl+"signUp/"+arg);
	}
}
