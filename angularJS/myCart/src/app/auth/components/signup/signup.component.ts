import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import  { Subject } from 'rxjs';
import  { debounceTime ,distinctUntilChanged , switchMap } from 'rxjs/operators';
import  { AuthService } from '../../auth.service';
  
@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

    private emailType = new Subject<string>();
    private emailDup = false;
    constructor(private authService :AuthService) { }

    ngOnInit() {
    }
    onSubmitForm(data : NgForm){
        this.authService.signUp(data.value);
        data.resetForm;
    }
    emailVerifyDuplicate(arg){
        this.emailType.next(arg);
    }
    public x = this.emailType.pipe(
        debounceTime(500),
        distinctUntilChanged(),
        switchMap((term) => this.authService.verifyEmail(term)),
    ).subscribe(data =>{
        this.emailDup = data.length == 0 ? false : true ;
    });
}
