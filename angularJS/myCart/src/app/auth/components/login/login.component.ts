import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { map , tap} from 'rxjs/operators';
import { Observable ,Subject , of} from 'rxjs';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	constructor(private service:AuthService , private router:Router) { }
	errortext = false;
	ngOnInit() {
		this.service.isLoggedIn() ? this.router.navigate(['/','products']):this.router.navigate(['/']);
	}
	onSubmitForm(data : NgForm){
		if(data.valid){
			this.service.login(data.value);
		}
		this.errortext = true;
	}
}
