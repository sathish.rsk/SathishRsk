import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule  } from '@angular/forms';

import { LoginComponent } from './components/login/login.component';
import { AppRoutingModule } from '../app-routing.module';
import { SignupComponent } from './components/signup/signup.component';

@NgModule({
  imports: [
		CommonModule,
		AppRoutingModule,
		FormsModule
  ],
  declarations: [LoginComponent, SignupComponent]
})
export class AuthModule { }
