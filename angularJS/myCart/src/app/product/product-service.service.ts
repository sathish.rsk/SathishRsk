import { Injectable } from '@angular/core';
import  Iproduct  from '../models/product';
import { HttpClient } from '@angular/common/http';
import { Observable} from 'rxjs';
import { map } from 'rxjs/operators';
import { AppService } from '../app.service';

@Injectable({
  providedIn: 'root'
})
export class ProductServiceService {
	productListData:Observable <Iproduct[]> ;
		constructor(private http: HttpClient ,
			private appService :AppService) {
	}
	getProductData() : Observable <Iproduct[]>{
		return this.http.get <Iproduct[]> ('http://localhost:8090/api/myCart/get/allProduct');
	}
	getProductById(productId):Observable <Iproduct>{
		return this.http
		.get <Iproduct> ('http://localhost:8090/api/myCart/get/product/'+productId);
		// .pipe(
		// 	map((products : Iproduct[]) => {
		// 		return products.find((p:Iproduct) => p._id === productId)
		// 	})
		// );
	}
	putCheckProduct(pdt : Iproduct){
		this.appService.addCheckOut(pdt);
	}
}
