import { Component, OnInit } from '@angular/core';
import { ProductServiceService } from '../../product-service.service';
import Iproduct from '../../../models/product';
import { Observable } from 'rxjs';
import { ActivatedRoute }  from '@angular/router';


@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
	product:Iproduct;
	constructor(private service: ProductServiceService ,private curUrl :ActivatedRoute) { }

	ngOnInit() {
		const productId = this.curUrl.snapshot.paramMap.get('productId');
		this.service.getProductById(productId).subscribe(data => {
			this.product = data;
		})
	}
	checkItem(arg){
		this.service.putCheckProduct(arg);
	}
}
