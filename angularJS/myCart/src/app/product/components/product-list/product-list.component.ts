import { Component, OnInit } from '@angular/core';
import { ProductServiceService } from '../../product-service.service';
import Iproduct from '../../../models/product';
import { Observable } from 'rxjs';


@Component({
	selector: 'app-product-list',
	templateUrl: './product-list.component.html',
	styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
	products$: Observable<Iproduct[]>;
	constructor(private service: ProductServiceService) { }

	ngOnInit() {
		this.products$ = this.service.getProductData();
	}
}