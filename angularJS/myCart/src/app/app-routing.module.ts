import { NgModule } from '@angular/core';
import { RouterModule , Routes} from '@angular/router';

import { ProductListComponent } from './product/components/product-list/product-list.component';
import { CartListComponent } from './cart/components/cart-list/cart-list.component';
import { ProductDetailComponent } from './product/components/product-detail/product-detail.component';
import { LoginComponent } from './auth/components/login/login.component';
import { CheckOutComponent } from './cart/components/check-out/check-out.component';
import  { SignupComponent } from './auth/components/signup/signup.component'

import { AuthGuard } from './auth/auth.guard';

const routes:Routes =[
	{
	  path:'products',
	  component: ProductListComponent
	},
	{
	  path:'cart',
		component: CartListComponent,
		canActivate:[AuthGuard]
	},
	{
	  path:'',
	  redirectTo:'/login',
	  pathMatch:'full'
	},
	{
		path:'productDetail/:productId',
		component:ProductDetailComponent
	},
	{
		path:'login',
		component:LoginComponent
	},
	{
		path:'checkout',
		component:CheckOutComponent,
		canActivate:[AuthGuard]
	},
	{
		path : "signup",
		component:SignupComponent
	}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  declarations: [],
  exports: [ RouterModule ]
})
export class AppRoutingModule { 

}
