import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';
import { AuthService } from './auth/auth.service';
 
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  implements OnInit{
	title = 'app';
	constructor(private service : AppService , private authService :AuthService){

	}
	logView = true;
	ngOnInit(){
		this.logView = this.authService.isLoggedIn();
		this.authService.logEvent.subscribe(data =>{
			this.logView = data;
		})
	}
	logoutFunc(){
		this.authService.logout();
	}
}
