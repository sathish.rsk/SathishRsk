//requires pre setup
const express = require('express');
const mongoose = require('mongoose');
const bParser = require('body-parser');

const myApp = express();
myApp.use(bParser.json());

const url = "mongodb://localhost:27017/testDB";
const options = {
    useNewUrlParser :true
};

myApp.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

//listen to the port 8090
myApp.listen(8090, function(){
    console.log("sucess happy !!!");
});

//initialize content
myApp.get('/', function (req, res) {
  res.send('Hello World')
});

//connect to DB
mongoose.connect(url , options).then(
  () => { console.log('connected'); },
  err => { console.log(err); }
);

//schema
var Schema = mongoose.Schema;
var cartSchema = new Schema({
        name:String,
        company:String,
        description:String,
        price:Number,
        imgUrl:String 
    },
    {
        collection : 'myCart'
    });

//create an model
const cartModel = mongoose.model('myCart', cartSchema);

//get Cart data from mongoDB
myApp.get('/api/myCart/get/allProduct',function(req, res){
    cartModel.find().then(snap =>{
        res.send(snap);
    }).catch(err => {
        res.send(err.message);
    }); 
});

myApp.post('/api/myCart/put/product',function(req , res){
    cartModel.insertMany(req.body, function(err , docs){
        if(err)
            res.send(err.message);
        else
            res.send(docs);
    });
});
myApp.get('/api/myCart/get/product/:productId',function(req , res){
    cartModel.findById(req.params.productId , function(err , docs){
        if(err)
            res.send(err.message);
        else
            res.send(docs);
    });
});

//my Auth functions
var userSchema = new Schema({
        name :String,
        email:String,
        password:String
    },
    {
        collection : 'users'
    }
);

//create an model
const userModel = mongoose.model('users', userSchema);

myApp.post('/api/myCart/signUp',function(req , res){
    userModel.insertMany(req.body, function(err , docs){
        if(err)
            res.send(err.message);
        else
            res.send(docs);
    });
});
myApp.get('/api/myCart/signUp/:email',function(req , res){
    userModel.find({
         email:req.params.email
    }).then(function(docs){
        res.send(docs);
    }).catch(function(err){
        res.send(err);
    });
});
myApp.post('/api/myCart/logIn',function(req , res){
    var regEmail = new RegExp(req.body.email);
    var regPassword = new RegExp(req.body.password);
    userModel.find({$and:
        [
            { email:{$regex: regEmail } },
            { password:{$regex: regPassword } }
        ]
    }).then(function(docs){
        res.send(docs);
    }).catch(function(err){
        res.send(err);
    });
});
