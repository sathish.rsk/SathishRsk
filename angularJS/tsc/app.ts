function arrChange(arr:number[]):Array<String>{
    return arr
        .filter((item: number): boolean => item < 10)
        .map((item: number): string => "item is "+item);
}
myArray();
function myArray():void{
  var arr: Array<number> = [10,5,2,12,13];
  console.log(arrChange(arr));
}