function arrChange(arr) {
    return arr
        .filter(function (item) { return item < 10; })
        .map(function (item) { return "item is " + item; });
}
myArray();
function myArray() {
    var arr = [10, 5, 2, 12, 13];
    console.log(arrChange(arr));
}
//# sourceMappingURL=app.js.map