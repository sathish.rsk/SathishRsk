//requires pre setup
const express = require('express');
const mongoose = require('mongoose');
const bParser = require('body-parser');

const myApp = express();
myApp.use(bParser.json());

const url = "mongodb://localhost:27017/testDB";
const options = {
    useNewUrlParser :true
};

myApp.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

//listen to the port 8090
myApp.listen(8090, function(){
    console.log("sucess happy !!!");
});

//initialize content
myApp.get('/', function (req, res) {
  res.send('Hello World')
});

//connect to DB
mongoose.connect(url , options).then(
  () => { console.log('connected'); },
  err => { console.log(err); }
);

//schema
var Schema = mongoose.Schema;
var blogSchema = new Schema({
        name : String    
    },{
        collection : 'testCollection'
    });

//create an model
const Blog = mongoose.model('Test', blogSchema);

//put data to an testDB
myApp.post('/api/create', function (req, res) {
    Blog.insertMany({name : req.body.name} , function(err, docs) {
        if(err){
            res.send({
                message: err.message || "Some error occurred while retrieving notes."
            });
        }else{
            res.send(docs);
        }
    }); 
});

//get data from db
myApp.get('/api/getAll', function(req , res){
    Blog.find().then(snap => {res.send(snap)}).catch(err => {
        res.send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
});
//update data from db
myApp.put('/api/UpdateByName', function(req , res){
    Blog.find({
        name : req.body.name
    }).then(function(snap){
        console.log(snap);
        Blog.findByIdAndUpdate(snap[0]._id, {
           name : req.body.update
        }, {new: true}).then(snap => {
            if(!snap) {
                res.status(404).send({
                    message: "Note not found with id " + snap._id
                });
            }else{
                res.send(snap);
            }
        }).catch(err => {
            res.send({
                message: err.message || "Some error occurred while retrieving notes."
            });
        });
    }).catch(err => {
        res.send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
});


//employee post function call
var empSchema  = new Schema({
        name:String,
        position:String,
        location:String,
        age:String,
        date:Date,
        salary:String
    },{
    collection:'myEmployee'
});
const myEmpPtr = mongoose.model('employee',empSchema);

myApp.post('/api/employee/post/details',function(req , res){
     myEmpPtr.insertMany(req.body, function(err, docs) {
        if(err){
            res.send({
                message: err.message || "Some error occurred while retrieving notes."
            });
        }else{
            res.send(docs);
        }
    });
});
myApp.get('/api/employee/get/details' ,function(req , res){
    var lim = parseInt(req.query.limit);
    var sortData = {};
    sortData[req.query.sname] = req.query.dir == "true" ? 1 : -1 ;
    if(req.query.search == ""){
        myEmpPtr.find()
        .limit(lim)
        .sort(sortData)
        .then(function(docs){
            res.send(docs);
        }).catch(function(err){
            res.send(err);
        });
    }
    else{
        var reg = new RegExp(req.query.search);
        myEmpPtr.find({$or:
            [
                {name:{$regex: reg, }},
                {position:{$regex: reg, }},
                {location:{$regex: reg,}},
                {age:{$regex: reg,}},
                {salary:{$regex: reg,}}
            ]
        })
        .limit(lim)
        .sort(sortData)
        .then(function(docs){
            res.send(docs);
        }).catch(function(err){
            res.send(err);
        });
    }
});
myApp.get('/api/employee/get/details/search',function(req , res){
    console.log(req.query);
});