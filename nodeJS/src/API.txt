###POST###
url = http://localhost:8090/api/employee/post/details
parameter : object <array of objects  (or) single object>
return   : sucess <status : 200 & object with objectID as _id>
            error <status : 400 (Bad Request) , 404 (NotFound)>

###GET###
url : http://localhost:8090/api/employee/get/details
parameter : params ( url encoded value ) <limit ( integer ) , search ( string )>
return : sucess <status : 200 & object>
         error <status : 400 (Bad Request) , 404 (NotFound)>