angular.module("myEmployee", ['employeeService'])

.controller("myEmployeeCtrl",function($scope , apiCall){
    $scope.positions = ["Accountant" , "Cheif Executive Officer(CEO)","Junior Technical Author","Software Engineer" , "Integration Specialist" ,"Pre-Sales Support" , "Sales Assistant" ,"Senior JavaScript Devloper"];
    $scope.locations = ["New York" , "India","Japan","Africa" , "Australia" ,"Dubai" , "London"];
    $scope.postDetails = function(){
        console.log($scope.data);
        $scope.postURL = "http://localhost:8090/api/employee/post/details";
        apiCall.postEmployee($scope.postURL , $scope.data).then(function(data){
            getemployeeData();
        }).catch(function(err){
            console.log(err);
        });
    }
    $scope.getDetails = function(){
        getemployeeData();
    }
    $scope.stripDate = function(date) {
      return date.substring(0,10);
    }
    $scope.stripSalary = function(salary){
        var s = salary.split("").reverse().join("").match(/.{1,3}/g);
        var t = "";
        for(var i = 0 ; i < s.length ; i++){
            s[i] = s[i].split("").reverse().join("");
        }
        return "$"+s.reverse().join(",");
    }
    $scope.limitData  = function(){
        getemployeeData();
    }
    $scope.searchData = function(){
        console.log();
        if($scope.search.length > 1){
            getemployeeData();
        }
    }
    $scope.sortData = "";
    $scope.sort = function(para){
        var d = !$scope.sortData.dir;
        $scope.sortData ={
            sname: para,
            dir : d
        };
        $('#sortDis div').each(function(item){
            if(this.innerText.indexOf(para) > -1)
                $(this).children().text("^");
            else
                $(this).children().text("")
        });

        getemployeeData();
    }
    function getemployeeData(){
        $('#myForm').addClass('d-none');
        $('#result').removeClass('d-none');
        $scope.getURL = "http://localhost:8090/api/employee/get/details";
        var pram = {};
        $scope.limit != ""  && $scope.limit != undefined? pram["limit"] = $scope.limit : pram["limit"] = 10;
        $scope.search != undefined ? pram["search"] = $scope.search :pram["search"] = "";
        if($scope.sortData != "" && $scope.sortData != undefined){
            pram["sname"] = $scope.sortData.sname;
            pram["dir"] = $scope.sortData.dir;
        }else{
            pram["sname"] = "name";
            pram["dir"] = true;
        }
        apiCall.getEmployees($scope.getURL , pram).then(function(res){
            if(res.data.length == 0){
                 $scope.myset = ["NO record Found"];
            }
            $scope.myset = res.data;
        }).catch(function(err){
            console.log(err);
        });
    }
});