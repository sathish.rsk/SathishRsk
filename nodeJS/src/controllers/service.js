var app=angular.module("employeeService", []);
app.service('apiCall', ['$http', function($http) {
    this.postEmployee = function(url , data){
        return $http.post(url , data ,{
            headers: { 'Content-Type': 'application/json'}
        });
    }
    this.getEmployees = function(url , pram){
        return $http({
            method: "GET",
            url : url,
            params : pram
        });
    }
}]);