# API creation with help of MongoDB and NodeJS

This module is intended to provide working examples of Employee list
features and APIs.modules are  simple well documented and modification friendly.
It purily made on top of javaScript Engine. 

## Getting Started
There are mainy two things are important to start livey environment of project.That API names shows the functionalities perfectly

### Prerequisites

For creating an Lively enviroment you should have Node js.Node isn’t a program that you simply launch like Word or Photoshop: you won’t find it pinned to the taskbar or in your list of Apps.
MongoDB should be installed along with the MongoCompass for GUI environment
```
https://nodejs.org/en/
https://www.mongodb.com/download-center?jmp=homepage#community
```

### Installing

1.Install examples for devlopers download to folders /employeeDetails/ and /nodeJS/ make sure its been inside your htdocs or publicHTML or webapps.

2.Open MongoDB Compass to view the DB. click connect to connect to that particulat DB on default port (27017) . Check on your browser url localhost:27017 " sucess : It looks like you are trying to access MongoDB over HTTP on the native driver port."

3.check node and npm using cdm node -v and npm -v 

4.Run the Your_Path/nodeJS/index.js form your admin as ( node index )

5.Run index.html in your_path/employeeDetails/index.html


## Deployment

Easy to install process everything doesnt require any additional dependencies

## Built With

* [NodeJs](https://nodejs.org/en/docs/) - NodeJS used
* [MongoDB](https://docs.mongodb.com/) - Database use Mongo Compass
* [npm](https://docs.npmjs.com/) - Dependency management

## Contributing

API is an open source project and we are very happy to accept community contributions. Please refer to [CONTRIBUTING.md] for details.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/SathishRsk/tags). 

## Authors

* **Sathish Rsk** - *Initial work* - (https://gitlab.com/sathish.rsk/SathishRsk.git)

See also the list of [contributors](https://github.com/SathishRsk/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* prithivi
* etc
