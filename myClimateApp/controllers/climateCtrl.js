angular.module("myAppClimate", ['weatherService'])

.controller("climateCtrl",function($scope , apiCall){
    var data = [];
    getCities();
    function getCities(){
        $scope.urlCus = "http://battuta.medunes.net/api/country/all/?key=f61275d5e7c88f0dc66a9ff4c817be2e";
        apiCall.getWeather($scope.urlCus)
        .then(function mySuccess(res){
           $scope.cityArray = res.data;
        }).catch(function(err){
            console.log(err.statusText);
            return null;
        });
    }
    var url = "http://api.openweathermap.org/data/2.5/", appID = "&APPID=6349876dd3ffb11e9e0b68cca97f4362" , myData = "";
    $scope.getCity = function(namePara){
        if(namePara != undefined){
            $scope.comURL = "";
            if(namePara.city != undefined){
                (isNaN(namePara.city))? $scope.comURL = url+"weather?"+"q="+namePara.city+appID :  $scope.comURL = url+"weather?"+"id="+namePara.city+appID ;
            }
            else{
                $scope.comURL = url+"find?"+"lat="+namePara.lat+"&lon="+namePara.long+"&cnt="+namePara.con+appID;
            }
            apiCall.getWeather($scope.comURL)
                .then(function mySuccess(response) {
                    $scope.resultWthr = response.data;
                }, function myError(response) {
                    console.log(response);
                });
            $scope.name = {};
        }
    }
});